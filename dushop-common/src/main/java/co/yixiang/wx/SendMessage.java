package co.yixiang.wx;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.Map;

/*
 *
 * 这是发送模板消息的类
 * 5.12
 *
 * */
@Component
public class SendMessage {


    public static String getAccessToken() {
        try {
            // 将Accesstoken拿到
            String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
            String appid = "wxc06e43c6add09677";
            String SECRET = "397639e2ba45674fca40dde52dbae8c1";
            url = url.replace("APPID", appid);
            url = url.replace("APPSECRET", SECRET);
            String content = HttpUtil.httpUrlConnect(url, null, "GET");
            Map<String, Object> map = HttpUtil.getAccessTokenByJsonStr(content);
            String accessToken = (String) map.get("access_token");
            System.err.println(accessToken);
            return accessToken;
        } catch (Exception e) {
            return null;
        }
    }

    public static String getOfficialAccountAccessToken() {
        try {
            // 将Accesstoken拿到
            String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
            String appid = "wxcca9999f9ae3cba5";
            String SECRET = "de03d8eed09b535de3fa0e502c5c2a31";
            url = url.replace("APPID", appid);
            url = url.replace("APPSECRET", SECRET);
            String content = HttpUtil.httpUrlConnect(url, null, "GET");
            Map<String, Object> map = HttpUtil.getAccessTokenByJsonStr(content);
            String accessToken = (String) map.get("access_token");
            System.err.println(accessToken);
            return accessToken;
        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String[] args) {
        String remarkStr="收货人：卢朝卫 电话：13896189609 湖北省 武汉市  大学城";
        //sendOfficialAccountOrderReminderMsgSuccess("105.62","88888888888","oS-H2v36A6M-lMDhKRyKrDb5K01o",DateUtil.getDateTimeStr(),remarkStr);
    }
    /**
     * @return
     * @Description 客户下单给商户发消息-公众号
     * @Author chen
     * @Date 2020/4/10
     * @Param buyDateTimeStr购买时间
     */

    public static boolean sendOfficialAccountOrderReminderMsgSuccess(String  price, String orderId, String openId,String payTime,String remarkStr,String payTypeStr) {
        String accessToken = getOfficialAccountAccessToken();
        try {
            JSONObject data = new JSONObject();
            JSONObject frist = new JSONObject();
            frist.put("value", "有新用户下单，请及时处理");
            frist.put("color", "#9b9b9b");
            data.put("frist", frist);
            JSONObject keyword1 = new JSONObject();
            keyword1.put("value", price);
            keyword1.put("color", "#9b9b9b");
            data.put("keyword1", keyword1);

            JSONObject keyword2 = new JSONObject();
            keyword2.put("value", payTypeStr);
            keyword2.put("color", "#9b9b9b");
            data.put("keyword2", keyword2);

            JSONObject keyword3 = new JSONObject();
            keyword3.put("value", orderId);
            keyword3.put("color", "#9b9b9b");
            data.put("keyword3", keyword3);

            JSONObject keyword4 = new JSONObject();
            keyword4.put("value", payTime);
            keyword4.put("color", "#9b9b9b");
            data.put("keyword4", keyword4);

            JSONObject remark = new JSONObject();
            remark.put("value", remarkStr);
            remark.put("color", "#9b9b9b");
            data.put("remark", remark);


            JSONObject message = new JSONObject();
            message.put("template_id", "UT61zmlZAY58jTQFdrhIf-9-dtDNPQazXytdHsb8oaQ");
            message.put("touser", openId);
            message.put("pagepath", "/pages/home/main");
            message.put("topcolor", "#FF0000");
            message.put("data", data);
            String httpurl = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + accessToken;
            String result = HttpUtil.jsonPost(httpurl, message);
            JSONObject json = new JSONObject(result);
            System.err.println(json.get("errcode"));
            if (json.get("errmsg").equals("ok")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }



    /**
     * @return
     * @Description 客户申请退款给商户发消息-公众号
     * @Author chen
     * @Date 2020/4/10
     * @Param buyDateTimeStr购买时间
     */
    public static boolean sendOfficialAccountApplyForRefundMsgSuccess(String userName,String orderId,String price,String applayTimeStr, String remarkStr,  String openId,String nameOfStore) {
        String accessToken = getOfficialAccountAccessToken();
        try {
            JSONObject data = new JSONObject();
            JSONObject frist = new JSONObject();
            frist.put("value", "用户"+userName+"发起退款申请，请及时处理");
            frist.put("color", "#9b9b9b");
            data.put("frist", frist);

            JSONObject keyword1 = new JSONObject();
            keyword1.put("value", orderId);
            keyword1.put("color", "#9b9b9b");
            data.put("keyword1", keyword1);

            JSONObject keyword2 = new JSONObject();
            keyword2.put("value", nameOfStore);
            keyword2.put("color", "#9b9b9b");
            data.put("keyword2", keyword2);

            JSONObject keyword3 = new JSONObject();
            keyword3.put("value", price);
            keyword3.put("color", "#9b9b9b");
            data.put("keyword3", keyword3);

            JSONObject keyword4 = new JSONObject();
            keyword4.put("value", price);
            keyword4.put("color", "#9b9b9b");
            data.put("keyword4", keyword4);

            JSONObject keyword5 = new JSONObject();
            keyword5.put("value", applayTimeStr);
            keyword5.put("color", "#9b9b9b");
            data.put("keyword5", keyword5);

            JSONObject remark = new JSONObject();
            remark.put("value", remarkStr);
            remark.put("color", "#9b9b9b");
            data.put("remark", remark);

            JSONObject message = new JSONObject();
            message.put("template_id", "mrTl8UyjbR3IpUTfNx3xYFS1VaX4NimIkyKvsR-GpPM");
            message.put("touser", openId);
            message.put("pagepath", "/pages/home/main");
            message.put("topcolor", "#FF0000");
            message.put("data", data);
            String httpurl = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + accessToken;
            String result = HttpUtil.jsonPost(httpurl, message);
            JSONObject json = new JSONObject(result);
            System.err.println(json.get("errcode"));
            if (json.get("errmsg").equals("ok")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    } /**
     * @return
     * @Description 客户申请退款后给商户发消息
     * @Author chen
     * @Date 2020/4/10
     * @Param
     */
    public static boolean sendApplyForRefundMsgSuccess(String applyDateTimeStr, String price, String orderId, String openId) {
        String accessToken = getAccessToken();
        try {
            JSONObject data = new JSONObject();
            JSONObject keyword1 = new JSONObject();
            keyword1.put("value", orderId);
            keyword1.put("color", "#9b9b9b");
            data.put("character_string1", keyword1);
            JSONObject keyword2 = new JSONObject();
            keyword2.put("value", price);
            keyword2.put("color", "#9b9b9b");
            data.put("amount2", keyword2);
            JSONObject keyword3 = new JSONObject();
            keyword3.put("value", "待处理");
            keyword3.put("color", "#9b9b9b");
            data.put("phrase3", keyword3);
            JSONObject keyword4 = new JSONObject();
            keyword4.put("value", applyDateTimeStr);
            keyword4.put("color", "#9b9b9b");
            data.put("date4", keyword4);
            JSONObject keyword5 = new JSONObject();
            keyword5.put("value", "请尽快处理");
            keyword5.put("color", "#9b9b9b");
            data.put("thing5", keyword5);
            JSONObject message = new JSONObject();
            message.put("template_id", "cdH3QiwEaPiZ4MzEMQyMG9YgJlUjxzhr6jk0Mizst2I");
            message.put("touser", openId);
            message.put("url", "");
            message.put("topcolor", "#FF0000");
            message.put("data", data);
            String httpurl = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken;
            String result = HttpUtil.jsonPost(httpurl, message);
            JSONObject json = new JSONObject(result);
            System.err.println(json.get("errcode"));
            if (json.get("errmsg").equals("ok")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }


    /**
     * @return
     * @Description 发货后给客户发信息
     * @Author chen
     * @Date 2020/4/10
     * @Param CourierNumber快递单号
     * courierServicesCompany快递类型
     */
    public static boolean sendDeliveryNoticeMsgSuccess(String goodName, String orderId, String openId, String courierNumber, String courierServicesCompany) {
        String accessToken = getAccessToken();
        try {
            JSONObject data = new JSONObject();
            JSONObject keyword1 = new JSONObject();
            keyword1.put("value", orderId);
            keyword1.put("color", "#9b9b9b");
            data.put("character_string1", keyword1);
            JSONObject keyword2 = new JSONObject();
            keyword2.put("value", goodName);
            keyword2.put("color", "#9b9b9b");
            data.put("thing2", keyword2);
            JSONObject keyword3 = new JSONObject();
            keyword3.put("value", courierNumber);
            keyword3.put("color", "#9b9b9b");
            data.put("character_string4", keyword3);
            JSONObject keyword4 = new JSONObject();
            keyword4.put("value", courierServicesCompany);
            keyword4.put("color", "#9b9b9b");
            data.put("phrase3", keyword4);
            JSONObject keyword5 = new JSONObject();
            keyword5.put("value", "请随时关注订单进度");
            keyword5.put("color", "#9b9b9b");
            data.put("thing6", keyword5);
            JSONObject message = new JSONObject();
            message.put("template_id", "rss09U98Z4ga5hLfZ6YEmKZ4ai5TtFHuNY26V_BY8Dc");
            message.put("touser", openId);
            message.put("page", "/pages/launch/main");
            message.put("topcolor", "#FF0000");
            message.put("data", data);
            String httpurl = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken;
            String result = HttpUtil.jsonPost(httpurl, message);
            JSONObject json = new JSONObject(result);
            System.err.println(json.get("errcode"));
            if (json.get("errmsg").equals("ok")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }

    /**
     * @return
     * @Description 退款申请成功后给客户发送消息
     * @Author chen
     * @Date 2020/4/10
     * @Param
     */
    public static boolean sendRefundSuccessfulMsgSuccess(String price, String goodName, String orderId, String openId) {
        String accessToken = getAccessToken();
        try {
            JSONObject data = new JSONObject();
            JSONObject keyword1 = new JSONObject();
            keyword1.put("value", "您的退款申请已经通过");
            keyword1.put("color", "#9b9b9b");
            data.put("thing1", keyword1);
            JSONObject keyword2 = new JSONObject();
            keyword2.put("value", orderId);
            keyword2.put("color", "#9b9b9b");
            data.put("character_string2", keyword2);
            JSONObject keyword3 = new JSONObject();
            keyword3.put("value", goodName);
            keyword3.put("color", "#9b9b9b");
            data.put("thing3", keyword3);
            JSONObject keyword4 = new JSONObject();
            keyword4.put("value", price);
            keyword4.put("color", "#9b9b9b");
            data.put("amount4", keyword4);
            JSONObject message = new JSONObject();
            message.put("template_id", "x-089hVaYcVLuhhd38_Uj_R1XK4YXRonqNg__uIdeoc");
            message.put("touser", openId);
            message.put("url", "");
            message.put("topcolor", "#FF0000");
            message.put("data", data);
            String httpurl = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken;
            String result = HttpUtil.jsonPost(httpurl, message);
            JSONObject json = new JSONObject(result);
            System.err.println(json.get("errcode"));
            if (json.get("errmsg").equals("ok")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }

    public static boolean sendBookSuccess(String accessToken) {
        try {

            JSONObject _data = new JSONObject();
            JSONObject keyword1 = new JSONObject();
            keyword1.put("value", "购买时间");
            keyword1.put("color", "#9b9b9b");
            _data.put("date2", keyword1);
            JSONObject keyword2 = new JSONObject();
            keyword2.put("value", "商品名称");
            keyword2.put("color", "#9b9b9b");
            _data.put("thing1", keyword2);
            JSONObject keyword3 = new JSONObject();
            keyword3.put("value", "201910250901");
            keyword3.put("color", "#9b9b9b");
            _data.put("number3", keyword3);
            JSONObject _message = new JSONObject();
            _message.put("template_id", "Y0db5_g5gBqoqIL6OHgP5AzRuouFrNkR_wCCc24AVeU"); // 模板id");
            _message.put("touser", "oMpKq5ZNmBk_NxnB6EOWeOFLiC0I");
            _message.put("url", "");
            _message.put("topcolor", "#FF0000");
            _message.put("data", _data);
            String httpurl = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken;
            String result = HttpUtil.jsonPost(httpurl, _message);
            JSONObject json = new JSONObject(result);
            System.err.println(json.get("errcode"));
            if (json.get("errmsg").equals("ok")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }

}
