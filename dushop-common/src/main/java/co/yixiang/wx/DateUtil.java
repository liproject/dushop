package co.yixiang.wx;

import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

@Slf4j
public class DateUtil {

    /**
     * 根据时间戳获取年月
     */
    public static String getDateTimeStr() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+08:00"));
        return new SimpleDateFormat("yyyy年MM月dd日 HH:mm").format(calendar.getTime());
    }



}
