package co.yixiang.wx;
import org.json.JSONObject;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

public class HttpUtil {
	public static String httpUrlConnect(String httpUrl, String params, String method) throws Exception {
		System.out.println("参数：=====" + params);
		URL url = new URL(httpUrl);
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setRequestProperty("accept", "*/*");
		urlConnection.setRequestProperty("connection", "Keep-Alive");
		urlConnection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
		urlConnection.setDoOutput(true);
		urlConnection.setDoInput(true);
		urlConnection.setRequestMethod(method);
		urlConnection.connect();
		PrintWriter out = null;
		BufferedReader in = null;
		if (null != params && !"".equals(params)) {
			out = new PrintWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "utf-8"));
			out.print(params);
			out.flush();
		}
		in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
		String line;
		String result = "";
		while ((line = in.readLine()) != null) {
			result += line;
		}
		System.out.println("结果："+result);
		return result;

	}

	/**
	 * 根据字符串json数据解析access_token
	 *
	 * @param jsonStr
	 * @return map
	 */
	public static Map<String, Object> getAccessTokenByJsonStr(String jsonStr) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			JSONObject jsonObj = new JSONObject(jsonStr);
			if (jsonObj.has("access_token")) {
				map.put("access_token", jsonObj.get("access_token"));
			}
			if (jsonObj.has("expires_in")) {
				map.put("expires_in", jsonObj.get("expires_in"));
			}
			return map;
		} catch (Exception e) {
			// TODO: handle exception
			return map;
		}
	}

	public static String jsonPost(String url, JSONObject json) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			out = new PrintWriter(conn.getOutputStream());
			// 发送请求参数

			out.print(json);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
}
